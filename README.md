FE - reset - testA
===================

*This CSS reset was tested element by element and defined as needed.*


----------


Default CSS Values for HTML Elements:
https://www.w3schools.com/cssref/css_default_values.asp

Appendix D. Default style sheet for HTML 4
https://www.w3.org/TR/CSS2/sample.html

Mozilla CSS
https://hg.mozilla.org/mozilla-central/file/tip/layout/style/res/html.css

WebKit CSS
http://trac.webkit.org/browser/trunk/Source/WebCore/css/html.css

CSS Tools: Reset CSS
http://meyerweb.com/eric/tools/css/reset/

HTML5 Reset Stylesheet
http://html5doctor.com/html-5-reset-stylesheet/

HTML5 Rendering
https://www.w3.org/TR/html5/rendering.html

\* { Box-sizing: Border-box } FTW
https://www.paulirish.com/2012/box-sizing-border-box-ftw/

Box Sizing
https://css-tricks.com/box-sizing/

